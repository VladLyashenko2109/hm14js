// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку “Змінити тему”.
// При натисканні на кнопку – змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання – 
// повертати все як було спочатку – начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки


document.querySelector('.change-theme').addEventListener('click', (event) => {
        event.preventDefault();
    if(localStorage.getItem('theme') === 'dark') {
        localStorage.removeItem('theme');          
    } else {
        localStorage.setItem('theme', 'dark');
    }
    addThemeToHTML();
});


 
function addThemeToHTML() {
    if(localStorage.getItem('theme') === 'dark') {
        document.querySelector('.backg-main').classList.add('dark');
    } else {
        document.querySelector('.backg-main').classList.remove('dark');
    }
}

addThemeToHTML();